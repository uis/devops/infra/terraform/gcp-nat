# GCP HA NAT

This module creates a regional HA NAT in a managed instance group (with target size
of 1), attached with a simple health check to ensure the gateways will
automatically restart if fails.

The NAT will have two network interfaces, one connected to a VPC Shared
network and one to the cluster network.

## Resources created

* `google_compute_address`: External IP for NAT instance
* `google_compute_instance_template`: Instance template to create NAT VMs
* `google_compute_firewall`: Firewall rules to allow ingress traffic from health check probes
* `google_compute_region_instance_group_manager`: Instance group manager that uses instance templates to create HA
* `google_compute_health_check`: A simple HTTP health check for monitoring responsiveness
