# External IP to reserve for NAT - used for health checks

resource "google_compute_address" "external_address" {
  project = var.project
  name    = "${var.name}-external"
  region  = var.region
}
