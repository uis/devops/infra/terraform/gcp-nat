# Simple example
This is a simple example of creating HA NAT in a given region.  

```hcl-terraform
$ terraform init
$ terraform apply -var 'project=<PROJECT_ID>' \
                  -var 'region=<REGION>' \
                  -var 'size=<SIZE>'
``` 
