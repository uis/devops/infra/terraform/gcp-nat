module "nat" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-nat.git"

  project = "foo"
  region  = "europe-west2"

  cluster_ip = var.cluster_ip

  shared_vpc_project    = var.shared_vpc_project
  shared_vpc_subnetwork = var.shared_vpc_subnetwork

  startup_script = file("startup.sh")
}
