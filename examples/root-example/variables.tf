# variables.tf defines inputs for the module

variable "project" {
  description = "Project ID where the resources will be created"
}

variable "region" {
  description = "Region where the resources will be created"
}

variable "cluster_ip" {
  description = "IP address of NAT instance"
}

variable "shared_vpc_project" {
  description = "Project that owns the Shared VPC network"
}

variable "shared_vpc_subnetwork" {
  description = "Subnet in Shared VPC network to use"
}
