resource "google_compute_firewall" "nat" {
  project = var.project
  name    = var.fw_name
  network = var.cluster_network

  # allow ingress from health check probes
  allow {
    protocol = var.fw_protocol
    ports    = var.fw_ports
  }

  source_ranges = var.fw_source_ranges

  target_tags = var.tags
}
