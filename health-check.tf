# Simple HTTP health check
resource "google_compute_health_check" "autohealing" {
  project = var.project
  name    = var.hc_name

  timeout_sec        = var.hc_timeout
  check_interval_sec = var.hc_interval

  healthy_threshold   = var.hc_healthy_threshold
  unhealthy_threshold = var.hc_unhealthy_threshold

  http_health_check {
    port         = var.hc_port
    request_path = "/health-check"
  }
}
