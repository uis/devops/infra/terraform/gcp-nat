resource "google_compute_region_instance_group_manager" "nat" {
  project            = var.project
  region             = var.region
  name               = "${var.name}-igm"
  base_instance_name = var.name

  target_size = 1

  version {
    name              = var.name
    instance_template = google_compute_instance_template.nat.id
  }

  auto_healing_policies {
    health_check      = google_compute_health_check.autohealing.id
    initial_delay_sec = var.hc_initial_delay
  }
}
