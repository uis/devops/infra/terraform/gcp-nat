# output.tf defines outputs for the module.

output "external_address" {
  description = "NAT External IP address"
  value       = google_compute_address.external_address
}

output "nat_firewall" {
  description = "Firewall rules for NAT"
  value       = google_compute_firewall.nat
}

output "nat_template" {
  description = "Instance template for NAT"
  value       = google_compute_instance_template.nat
}

output "nat_healthcheck" {
  description = "Health check for NAT"
  value       = google_compute_health_check.autohealing
}
