#!/bin/bash

# Enable IP forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/20-natgw.conf

# Get NICs (NIC1 = VPC Shared, NIC2 = Cluster)
NICS=$(ifconfig | grep flags | head -2 | awk -F: {'print $1'})
NIC1=$(echo $NICS | awk {'print $1'})
NIC2=$(echo $NICS | awk {'print $2'})

# VPC needs to masquerade
iptables -t nat -A POSTROUTING -o $NIC1 -j MASQUERADE

# Forward all packets from cluster to Shared VPC, and only established
# packets the other way
iptables -A FORWARD -i $NIC2 -o $NIC1 -j ACCEPT
iptables -A FORWARD -i $NIC1 -o $NIC2 -m state --state RELATED,ESTABLISHED -j ACCEPT

# Create basic health check server
cat <<EOF > /usr/local/sbin/health-check-server.py
#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import subprocess

PORT_NUMBER = 80
PING_HOST = "www.google.com"

def connectivityCheck():
  try:
    subprocess.check_call(["ping", "-c", "1", PING_HOST])
    return True
  except subprocess.CalledProcessError as e:
    return False

#This class handles any incoming request
class myHandler(BaseHTTPRequestHandler):
  def do_GET(self):
    if self.path == '/health-check':
      if connectivityCheck():
        self.send_response(200)
      else:
        self.send_response(503)
    else:
      self.send_response(404)

try:
  server = HTTPServer(("", PORT_NUMBER), myHandler)
  print "Started httpserver on port " , PORT_NUMBER
  #Wait forever for incoming http requests
  server.serve_forever()

except KeyboardInterrupt:
  print "^C received, shutting down the web server"
  server.socket.close()
EOF

nohup python /usr/local/sbin/health-check-server.py >/dev/null 2>&1 &
