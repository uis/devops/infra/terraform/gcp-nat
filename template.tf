# Instance template to create a NAT VM with two network interfaces

resource "google_compute_instance_template" "nat" {
  project     = var.project
  name_prefix = "${var.name}-"

  lifecycle {
    create_before_destroy = true
  }

  machine_type            = var.machine_type
  can_ip_forward          = var.can_ip_forward
  metadata_startup_script = coalesce(var.startup_script, file("${path.module}/scripts/startup.sh"))

  disk {
    source_image = data.google_compute_image.nat_image.self_link
  }

  # Network interface for Shared VPC Network comes first and subnet is specified
  # but let the IP be auto allocated
  network_interface {
    subnetwork_project = var.shared_vpc_project
    subnetwork         = var.shared_vpc_subnetwork

    access_config {
      # External IP of NAT
      nat_ip = google_compute_address.external_address.address
    }
  }

  # Network interface for the GKE cluster needs the network and IP to be specified
  network_interface {
    network    = var.cluster_network
    network_ip = var.cluster_ip
  }

  tags = var.tags

  depends_on = [
    google_compute_address.external_address
  ]
}

data "google_compute_image" "nat_image" {
  family  = var.image_family
  project = var.image_project
}
