# variables.tf defines inputs for the module

variable "project" {
  description = "Project ID where the resources will be created"
}

variable "region" {
  description = "Region where the resources will be created"
}

variable "name" {
  default     = "nat"
  description = "Name of NAT VM instance and associated components"
}

variable "cluster_network" {
  default     = "default"
  description = "Cluster network to attach NAT to"
}

variable "cluster_ip" {
  description = "IP address of NAT in cluster"
}

variable "shared_vpc_project" {
  description = "Project ID of the Shared VPC project"
}

variable "shared_vpc_subnetwork" {
  description = "Subnet to attach NAT to in the Shared VPC project"
}

variable "tags" {
  default     = ""
  description = "Tags added to NAT for firewall and networking"
}

variable "can_ip_forward" {
  default     = true
  description = "Allow IP forwarding"
}

variable "machine_type" {
  default     = "n1-standard-1"
  description = "Machine type for NAT in the instance group"
}

variable "startup_script" {
  default     = ""
  description = "Content of startup-script metadata passed to the instance template"
}

variable "image_family" {
  default     = "debian-10"
  description = "Image used for NAT"
}

variable "image_project" {
  default     = "debian-cloud"
  description = "Image project used for NAT"
}

variable "hc_name" {
  default     = "nat-health-check"
  description = "Name of health check"
}

variable "hc_timeout" {
  default     = 5
  description = "How long (in seconds) to wait before claiming failure"
}

variable "hc_interval" {
  default     = 30
  description = "Check interval in seconds"
}

variable "hc_healthy_threshold" {
  default     = 1
  description = "Healthy threshold"
}

variable "hc_unhealthy_threshold" {
  default     = 5
  description = "Unhealthy threshold"
}

variable "hc_initial_delay" {
  default     = 30
  description = "Initial delay in seconds"
}

variable "hc_port" {
  default     = 80
  description = "Port for health check request"
}

variable "fw_name" {
  default     = "natfirewall"
  description = "Name of firewall rule"
}

variable "fw_protocol" {
  default     = "tcp"
  description = "Protocol of firewall rule"
}

variable "fw_ports" {
  default     = ["80"]
  description = "Ports of firewall rule"
}

variable "fw_source_ranges" {
  # Default source IP ranges for health checks for TCP/UDP, HTTP(S), TCP Proxy, SSL Proxy
  default     = ["130.211.0.0/22", "35.191.0.0/16"]
  description = "The source IP ranges of health check probes"
}
